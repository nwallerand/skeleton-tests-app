// https://github.com/cucumber/cucumber-js/blob/main/docs/configuration.md
module.exports = {
    default: {
        parallel: 2,
        format: ['html:cucumber-report.html'],
        publishQuiet: true
    }
}
