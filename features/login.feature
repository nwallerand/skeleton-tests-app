Feature: Login
  As a user
  In order to access my account
  I want to be able to log in

  Scenario: Successful login
    Given I am on the login page
    When I enter my username and password
    And I click on the "Login" button
    Then I should be logged in

  Scenario: Invalid login
    Given I am on the login page
    When I enter an invalid username or password
    And I click on the "Login" button
    Then I should see an error message
    And I should not be logged in
