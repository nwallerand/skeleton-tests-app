const { Given, When, Then, AfterAll } = require('@cucumber/cucumber')
const { expect } = require('chai');
const webdriver = require('selenium-webdriver');
const fs = require('fs');
const { By, until, Builder, Capabilities } = webdriver

require("chromedriver");

// driver setup
const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { "w3c": false });

const driver = new Builder().usingServer('http://selenium-hub:4444/wd/hub').withCapabilities(capabilities).build();

/*
webdriver.WebDriver.prototype.saveScreenshot = function (filename) {
    return this.takeScreenshot().then(function (data) {
        fs.writeFile(`${__dirname}/screenshots/${filename}`, data.replace(/^data:image\/png;base64,/, ''), 'base64', function (err) {
            if (err) throw err;
        });
    });
};*/

Given('I am on the login page', async function () {
    await driver.get('https://app.almana.io/fr/login');
});

When('I enter my username and password', async function () {
    // Saisie du nom d'utilisateur et du mot de passe
    await driver.findElement(By.id('inputEmail')).sendKeys('fdfdfdfddfdfd');
    // await driver.findElement(By.id('password')).sendKeys('fgfgfdgfdgfdf');
});


When('I click on the "Login" button', async function () {
    // Clic sur le bouton de connexion
    // await driver.findElement(By.id('login-button')).click();
});

When('I enter an invalid username or password', async function () {
    // await driver.findElement(By.id('username')).sendKeys('invalidusername');
    // await driver.findElement(By.id('password')).sendKeys('invalidpassword');
});

Then('I should be logged in', async function () {
    // Vérification de la réussite de la connexion
    // await driver.wait(until.titleIs('Welcome to the Dashboard'), 5000);
    // const welcomeMessage = await driver.findElement(By.css('.welcome-message')).getText();
    // expect(welcomeMessage).to.equal('Welcome, User!');
});

Then('I should see an error message', async function () {
    // await driver.wait(until.elementLocated(By.css('.error-message')), 5000);
    // errorMessage = await driver.findElement(By.css('.error-message')).getText();
    // expect(errorMessage).to.not.be.empty;
});

Then('I should not be logged in', async function () {
    // await driver.wait(until.titleIs('Login Page'), 5000);
    // const loginButton = await driver.findElement(By.id('login-button')).getText();
    // expect(loginButton).to.equal('Login');
});
/*

AfterAll(async () => {
  await driver.quit();
}, 40000);
*/