export SHELL := /bin/bash -euo pipefail
export UID := $(shell id -u)
export GID := $(shell id -g)

# Misc
.DEFAULT_GOAL = help
.PHONY        : help

help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9\./_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

start:
	docker compose up -d

test: start
	sleep 2
	docker compose run --rm node npm run cucumber
