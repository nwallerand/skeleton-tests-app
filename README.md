Pour build l'image NODE
```bashbash
docker compose build --no-cache node
```

Pour faire un npm install (avec le docker node) :
```bash
docker compose run --rm node npm install
```

Pour lancer la stack
```bash
docker compose up -d
```

Pour executer les tests "web-driver"
```bash
docker compose run --rm node npm run cucumber
```
